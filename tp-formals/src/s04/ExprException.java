package s04;

@SuppressWarnings("serial")
public class ExprException extends Exception {
  public ExprException()           {
    super();
  }
  public ExprException(String msg) {
    super(msg);
  }
}
