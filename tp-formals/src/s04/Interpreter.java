package s04;

import java.util.HashMap;

public class Interpreter {
  private static Lexer lexer;
  private static HashMap<String, Integer> ref = new HashMap<>();
  // TODO - A COMPLETER (ex. 3) 
  
  public static int evaluate(String e) throws ExprException {
    lexer = new Lexer(e);
    int res = parseExpr();
    // test if nothing follows the expression...
    if (lexer.crtSymbol().length() > 0)
      throw new ExprException("bad suffix");
    return res;
  }

  private static int parseExpr() throws ExprException {
    int res = 0;
    res = parseTerm();
    while(true) {
      if(lexer.isPlus()) {
        lexer.goToNextSymbol();
        res += parseTerm();
      } else if(lexer.isMinus()) {
        lexer.goToNextSymbol();
        res -= parseTerm();
      } else {
        break;
      }
    }
    // TODO - A COMPLETER
    return res;
  }

  private static int parseTerm() throws ExprException {
    int res = 0;
    res = parseFact();
    while(true) {
      if(lexer.isStar()) {
        lexer.goToNextSymbol();
        res *= parseFact();
      } else if(lexer.isSlash()) {
        lexer.goToNextSymbol();
        res /= parseFact();
      } else {
        break;
      }
    }
    // TODO - A COMPLETER
    return res;
  }

  private static int parseFact() throws ExprException {
     int res = 0;
     boolean par = false;
     if(lexer.isOpeningParenth()) {
       lexer.goToNextSymbol();
       res = parseExpr();
       if(!lexer.isClosingParenth()) throw new ExprException("Need a closing parenth");
       lexer.goToNextSymbol();
       par = true;
     } else if(lexer.isNumber()){
       res = lexer.intFromSymbol();
       lexer.goToNextSymbol();
     } else if(lexer.isIdent()) {
       String fnct = lexer.crtSymbol();
       lexer.goToNextSymbol();
       if(!lexer.isOpeningParenth()) {
         if(!ref.containsKey(fnct)){
           throw new ExprException("Need an opening parenth");
         }
         res = ref.get(fnct);
       } else {
         lexer.goToNextSymbol();
         int arg = parseExpr();
         res = applyFct(fnct, arg);
         if(!lexer.isClosingParenth()) throw new ExprException("Need a closing parenth");
         lexer.goToNextSymbol();
         par = true;
       }
     } else {
       throw new ExprException("Something was expected but there is nothing");
     }
     if(par && lexer.isIdent()){
       ref.put(lexer.crtSymbol(), res);
       lexer.goToNextSymbol();
     }
    // TODO - A COMPLETER
    return res;
  }

  private static int applyFct(String fctName, int arg) throws ExprException {
    switch(fctName){
      case "abs":
        return Math.abs(arg);
      case "sqr":
        return (int)Math.pow(arg, 2);
      case "cube":
        return (int)Math.pow(arg, 3);
      case "sqrt":
        if(arg<0) throw new ExprException("sqrt() accepts only positive number");
        return (int)Math.sqrt(arg);
      default: throw new ExprException("Not a kown fctName");
    }
  }
}
