package s05;

public class Ex5 {
  // Compute ...  TODO  (b), informal description
  // Retourne la racine cubique entrière de a

  static int cicr(int a) {
    assert a >= 0;
    int t=0, c=0, r=0; 
    while(c <= a) {
      assert c == r*r*r;
      assert t == 3*r;
      t = t + 3;
      assert t-3 == 3*r;  // TODO
      c = c + r*t + 1;
      assert c-r*t-1 == r*r*r;  // TODO
      r = r + 1; // c-(r-1)*t-1 == (r-1)^3
      assert c-(r-1)*3*r-1 == (r-1)*(r-1)*(r-1); // -> c == r^3
      assert t-3 == 3*(r-1); // -> t/3-1 == r-1 -> t == 3r
      assert c == r*r*r;
      assert t == 3*r;
    }
    assert (r-1)*(r-1)*(r-1) <= a && a < r*r*r == true;  // TODO (b), describe (r-1) formally
    System.out.println("a: " + a + " r-1: " + (r-1) + " c: " + c + "  t: " + t);
    return r-1;
  }
  
  //------------------------------------------------------------------------
  
  public static boolean areAssertionsEnabled() {
    int ec=0; 
    assert (ec=1) == 1;
    return ec == 1; 
  }
  
  public static void main(String[] args) {
    if(!areAssertionsEnabled()) {
      System.out.println("Please enable assertions, with '-ea' VM option !!");
      System.exit(-1);
    }
    // Call cicr() with various parameters, just to see if assertions agree... 
    int max = 570;
    for(int i=0; i<max; i++) 
      cicr(i);
    System.out.println("End of demo!");
  }
  
}
