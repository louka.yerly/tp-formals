package s05;

public class EX2 {



    private static int powerA(int b, int p) { // PRE: p > 0
        int res = 1, n = p;
        int x = b;              // x^n * res == b^p
        while (n>0) {           // n>0                     (n/2)*(sqrt(n)+n)
            if (n % 2 == 0) {   //
                x = x*x;        // x^((1/2)*(n)) * res = b^p && n>0
                n = n/2;        // x^((1/2)*(2n)) * res = b^p == x^n
            } else {            //
                res = res * x;  // x^(n-1) * res = b^p
                n--;            // x^(n-1+1) * res = b^p == x^n
            }
                                // x^n * res == b^p
        }
        return res; // n == 0 -> x^(n) * res == 1 * res == b^p
    }

    // CE N'EST PAS UN INVARIANT
    private static int powerB(int b, int p) { // PRE: p > 0
        int res = 1, n = p;
        int x = b;              // x^p * res == b^n
        while (n>0) {           // n>0
            if (n % 2 == 0) {   //
                x = x*x;        // x^((1/2)*(p)) * res = b^n && n>0
                n = n/2;        // x^((1/2)*(p)) * res = b^p == x^n
            } else {            //
                res = res * x;  // x^(n-1) * res = b^p
                n--;            // x^(n-1+1) * res = b^p == x^n
            }
            // x^n * res == b^p
        }
        return res; // n == 0 -> x^(n) * res == 1 * res == b^p
    }




    public static void main(String[] args){
        System.out.println(powerA(3,9));
    }






}
